Changelog
=========

0.11 (unreleased)
-----------------

- Add template for Plone 5.1 sites. Cleaned up, generate default add'ons yourself with bobtemplates.plone. [fred]

- Plone 4.3.15. [fred]

- Plone 5: Hotfix. Fix varnish-clearcache, add projectrelease variable, set Plone 5.0.6 find-links [fred]

- Plone 5.0.6.  [maurits]

- Plone 4.3.11.  [maurits]

- Use Plone 4.3.10. Update setuptools/zc.buildout. Add requirements.txt. Update README.txt for using virtualenv & pip. [fred]

- Use Plone 5.0.5. Update setuptools/zc.buildout . Add requirements.txt. Update README.txt for using virtualenv & pip. [fred]

- Update varnish in Plone 5 buildout to Varnish 4.1 and use plone.recipe.varnish [fred]

- Update to Plone 4.3.9.  Note: 4.3.8 and 4.3.9 have a TinyMCE that
  does not work nicely with our newer jquery.  We would need to update
  to jquery 1.12.3.  This is NOT yet done here.  [maurits]

- Update Plone 5 buildout with Plone 5.0.4
  [fred]

- Replace nodejs.cfg in plone4/5 buildouts with nodeenv based approach (was gp.recipe.node). gp.recipe.node is fine for single tool installs like csslint/jslint, but it patches the scripts it places in bin to correct node environment and settings. nodeenv creates a virtualenv like isolated nodejs environment. Then tools like npm shrinkwrap also work correctly.
  [fred]

- Update to Plone 4.3.8.
  [fred]

- Fix nodejs.cfg for Plone 4 template, adding the correct theme package names
  from project, clean up non template versions.
  [fred]

- Remove zodb_cache_size_bytes. It is questionnable if this doesn't cost
  performance in the Zope Server. Tweaking zodb-cache-size and monitoring
  memory resource usage is better.
  [fred]

- Plone 4 buildout: added nginx rule for denying manage_*.  Not needed
  on Plone 5 as we have good csrf protection out of the box there.
  [maurits]

- Updated iPython.
  [maurits]

- Plone 5: use current latest zc.buildout and setuptools.
  [maurits]

- Updated to Plone 5.0 final.
  [maurits]

- Updated to Plone 4.3.7.
  [maurits]

- Setup new theme structure and updated nodejs.cfg.
  [jladage]

- Various small changes so preview and production work, and
  install works.  devel has problems with nodejs.cfg though.  It
  cannot find bower and grunt.
  [maurits]

- Update buildouts with nodejs.cfg and included it in devel.cfg parts.
  [jladage]

- Update plone-versions.cfg to Plone 5.0rc3
  [jladage]

- Add robots part which disallows all bots to include in the preview.cfg.
  [jladage]

- Clean up: remove diazoframeowk.plone. Remove infrae.maildrophost. (Plone 4's
  maildrophost has transaction/spooling support).
  [fred]

- Copy the Plone 4 template structure to create a Plone 5 buildout.
  [fred]

- Revert back to zc.buildout 2.2.1.  Otherwise we would need
  setuptools 3.3 or higher (I would suggest 7.0), which could be fine,
  but may be too big a step.
  [maurits]

- Update to Plone 4.3.4

- zc.buildout 2.2.5
  [maurits]

- Use newest https://bootstrap.pypa.io/bootstrap-buildout.py with
  --setuptools-version option.
  [maurits]

- Remove newest=false from base.cfg.  This had bad side effects with
  the new setuptools 8.
  [maurits]

- Update Varnish to 3.0.6
  [fred]

- Add compass and varnish-clearcache parts.
  [jladage]

- Use collective.recipe.backup 2.20 with enable_zipbackup=true.
  [maurits]

- plone4: add project.content and project.policy packages.
  [maurits]

- plone4: Plone 4.3.3.  And update a few packages in versions.cfg.
  [maurits]

- plone4: update the generated readme and remove buildout.txt.  Let
  the readme point to online documentation instead.
  [maurits]

- plone4: make templates/varnishd.in executable after copying.
  [maurits]

- plone4: Add more to generated .gitignore.
  [maurits]

- plone4: override zeoclient and zeoserver ports in devel.cfg, as
  production.cfg directly uses the ports from base.cfg.
  [maurits]

- plone4: add allow-hosts option, to avoid trying to download from
  non-standard locations.
  [maurits]

- plone4: Automatically create a .gitignore (gitignore is moved to .gitignore)
  after template creation.
  [fred]

- plone4: update varnish to 3.0.5. Move varnish section to the base.cfg Move
  all port definitions to base.cfg as well.
  [fred]

- plone4: Use collective.recipe.backup 2.17, add alternative restore
  location for preview and add zipbackup part.
  [maurits]

- Add Products.PloneHotfix20131210.
  [maurits]

- plone4: Add persistent logging to supervisord processes. Use sane default log
  limits.
  [fred]

- plone4: Configure backup directory for preview properly. Update setuptools to
  1.4.2
  [fred]

- Remove Products.PloneHotfix20130618 from the eggs in the zest_buildout_plone4,
  4.3.2 includes it.
  [fred]

- zest_buildout_plone4: Plone 4.3.2.
  [maurits]

- Fix local_settings, we can't set a subkey in a dictionairy without
  moving all multi database settings into  a separate file. not for now.
  [fred]

- Move the import of local_settings from settings.py in the project
  directory to development/preview/production.py to avoid cumbersome
  python import problems if you only want to set the password for
  a database connection in local_settings.py
  [fred]

- Update setuptools to 1.1.4 (for Plone 4 buildout)
  [fred]

- collective.recipe.backup = 2.14
  [maurits]

- Updated diazotheme folder with the latest way of theming, based on TH-code's
  diazoframework.plone
  [thomvl]

- zc.buildout = 2.2.0
  distribute = 0.7.3
  setuptools = 0.8
  [maurits]


0.10 (2013-07-05)
-----------------

- Moved to bitbucket.
  [maurits]

- collective.recipe.backup 2.12.
  [maurits]

- Update to Products.PloneHotfix20130618 1.3.
  [maurits]

- Update to Plone 4.3.1, Add Products.PloneHotfix20130618 version 1.1 . Update
  default-addons.cfg with latest versions.
  [fred]

- Update to Plone 4.3 and added a boilerplate theme package in src.
  [jladage]

- Update Plone to 4.2.5 . Add pythonunbuffered env variable to supervisord
  startup. Delay httpok for two minutes afer startup in supervisor.
  [fred]

- Update debugtools iPython to 0.13.2 for Plone 4/Python 2.6.
  [fred]

- Add a list of default 3rd party addons we frequently use to default-addons.cfg
  that can be added/extended upon.
  [fred]

- show-picked-versions = true
  [maurits]

- newest = false
  [maurits]

- Set always-checkout = true.
  [maurits]

- Fix varnish.vcl.in for caching too aggressively logged in users.
  [fred]

- Fix varnishd startup and note that executable flags might not be transferred
  in the paster template.
  [fred]

- Update bootstrap/zc.buildout to 2.0.1 . Remove buildout-versions. Update
  several buildout recipes.
  [fred]

- All buildouts: fresh bootstrap.py that requires zc.buildout<2.0dev.
  [maurits]

- Updated zest_buildout_plone4 to Plone 4.2.4.

- Fixed aliases, pcre, admin_email definitions.
  [maurits]

- Add aliases variable for nginx config template.
  [jladage]

- Increase cache size for Plone 4 a bit and increase object cache size to
  50000 because we're limiting by bytes now anyway.
  [fred]

- Update bootstrap.py/zc.buildout/distribute to latest versions.
  [fred]

- Change varnish to without plone.recipe.varnish but stand alone runner and
  vcl files.
  [fred]

- Update minor packages (recipe.supervisor, hexagonic.recipe.*, supervisord),
  Add latest PloneHotfix 1.2 to Plone 4.2.2.
  [fred]

- Update to Plone 4.2.2.
  [maurits]

- Set zope_i18n_allowed_languages to Dutch and English by default.
  Saves about 50 MB of memory.
  [maurits]

- collective.recipe.backup = 2.7.
  [maurits]

- Comment out collective.loremipsum as it can break on startup.
  [maurits]

- Update to Plone 4.2.1.
  [maurits]

- Update a few versions in debugtools.cfg.
  [maurits]

- Add collective.loremipsum to the development tools.  Install it
  locally to create some dummy content using the Populate tab.
  [maurits]

- collective.recipe.backup = 2.6
  [maurits]

- Define ajax_load as ``request.form.get('ajax_load')`` in
  manifest.cfg.  For instance, the login_form has an hidden empty
  ajax_load input, which would give an unthemed page after submitting
  the form.
  [maurits]

- Update Zest django buildout with supervisor not using tcp ports anymore but
  sockets as well to communicate between the daemon and supervisorctl
  [fred]

- Updated Zest django buildout based on Fred's changes for Overwater:
  - fixes for the backup task.
  - new preview.cfg and template.
  [vincent]

- Update to Plone 4.2 Yeah! :-)
  [fred]

- Zope 2.13.15 with security and zeo fix for Plone 4.2
  [fred]

- Use Plone 4.2rc2.
  [fred]

- New zest_django_buildout version with Django 1.4. [vincent]

- Use bootstrap.py from zc.buildout 1.4.4 in the generated buildouts.
  This seems to help for avoiding permission errors when installing
  for the first time; it no longer tries to overwrite a system-wide
  setuptools.
  [maurits]

- Removed theming.cfg, as Plone 4.2b2 already has these versions
  (though using p.a.theming 1.0b8 at the moment).
  [maurits]

- Use Plone 4.2b2.
  [maurits]

- Use plone.app.theming 1.0b9:
  http://good-py.appspot.com/release/plone.app.theming/1.0b9
  [maurits]

- Update bin/test in Plone 4 to exit with status.  Also added a remark
  on its usage: bin/test -s packagename
  [maurits]

- Removed ez_setup svn external.
  [maurits]

- Update buildout.dumppickedversions to 0.5
  [fred]

- Use Plone 4.1.4
  [fred]

- Remove old hotfixes from the Plone 3 and Plone 4 buildout eggs, we use the
  latest releases now. Update superlance 0.6 in the Plone 4 buildout.
  [fred]

- On Plone 3 use own release of kss.core (and kss.demo) that works
  with Firefox 4 (base2 dom).
  [maurits]

- Upcate zc.recipe.cmmi to 1.3.5.
  [fred]

- Update Plone 3 buildout to release 3.3.6 & several other packages.
  [fred]

- Use collective.recipe.backup 2.4.
  [maurits]

- Update plone4 buildout to release 4.1.3
  [fred]

- Remove supervisor port definition in [conf] and switch to a unix socket in
  <buildoutdir>/var/supervisor.sock .
  [fred]

- Fix backupbloblocation for collective.recipe.backup in our Plone 4 recipe.
  It should be blobbackuplocation.
  [fred]

- increase nginx upload body limit from 10 to 30mb.
  [fred]

- Update varnish in plone 4 recipe to install pcre locally.
  [fred]

- Updated varnish cache download location in Plone 3 recipe.
  [fred]

- Updated Plone 4 buildout to fresh Plone 4.1.2.
  [maurits]

- Use collective.recipe.backup 2.3.
  [maurits]

- Update django buildout with latest Django version + fix problem with
  the new recipe that asks the version to be pinned normally. [vincent]

- Added preview.cfg.
  [maurits]

- Added option redirect_www that influences a few parts of the nginx
  configs.  Taken over from minaraad.
  [maurits]

- Updated to Plone 4.1
  [maurits]

- Update nginx config for redirect to cms.* editing website and redirect
  login_form and require_login in the Plone 4 buildout.
  [fred]

- Added plone.app.theming (commented out) to zest_buildout_plone4.
  Please read instructions after buildout is created.  Could be done
  more elegantly with an extra question and some conditions probably,
  but good enough for now.  Note that the sample theme in
  resources/theme is just an example (taken from vdSluis mostly, with
  images and css removed).
  [maurits]

- Update zest_buildout_plone4 to use Plone 4.1rc3, and cleanup versions.

- Add DCWorkflowGraph to Plone 4 buildout as well
  [fred]

- Update collective.recipe.supervisor, remove pinning for Plone 3 caching
  Products in the Plone 4 buildout
  [fred]

- Update several packages for the Plone 4.0 buildout. (plone.app.imaging,
  supervisor, zc.buildout, distribute)
  [fred]

- Update Plone4 buildout to Plone 4.0.7
  [fred]

- Deliver mail via maildrophost to localhost smtp.
  [fred]

- Update Plone4 buildout to Plone 4.0.5
  [fred]

- Add an nginx.cfg buildout configuration that you can include for local
  debugging purposes.
  [fred]

- Update svn ignores list that is printed at the end of the buildout.
  [fred]

- Add Products.PloneHotfix20110720 to the buildouts.
  [fred]

- Update TinyMCE to 1.23 for Plone4  and 1.1.8 for Plone 3. Remove Cachesetup
  and friends from the versions.cfg in Plone 4.
  [fred]

- Set zserver-threads by default to 3 because 2 gives a higher risk on
  deadlocks.
  [fred]

- Use TinyMCE 1.1.5 for Plone 3 and 1.2.0 for Plone 4
  [fred]

- Add DCWorkflowGraph to the devel products for Plone 3 and 4 buildouts
  [fred]

- Use plone.recipe.varnish 1.2
  [maurits]

- Use collective.recipe.backup 1.7.
  [maurits]

- Update Plone 4 buildout to 4.0.2. Products.TinyMCE 1.1.5, Plone 4 buildout
  was still using older supervisor.
  [fredvd]

- Use fresh collective.recipe.omelette 0.10.
  [maurits]

- Use z3c.recipe.usercrontab 1.1.
  [mark]

- Add the "-q" parameter to the backup cronjob of Plone sites since we
  don't need emails every night that the backup succeeded.
  [mark]

- Add zodb-cache-size-bytes to the base.cfg as well in the Plone 4 buildout.
  [fred]

- Add zodb-cache-size-bytes setting for production in Plone 4, default to 64Mb
  per thread. [fred]

- add shared-blob=on so that production will work for the plone templates
  [vincent+fred]

- add buildout.cfg.orig templates to the plone skeletons that you can copy to
  buildout.cfg [fred]

- Pin supervisor to 3.0a9, update mr.developer to 1.16. [fred]

- Set devel.cfg to extend the buildout extensions, otherwise our mr.developer
  from base.cfg is not being used. [fred]

- Update port numbers to match the new scheme. [mark]

- Updated zest_buildout_plone4 with Plone 4.0.1 pinnings. [fred]

- Added redirection when user forget www in nginx conf for django buildout.
  [vincent]

- Updated django_buildout template so it add supervisor, backup and nginx
  templating. [vincent]

- Fix nginx.conf.in template. [mark]

- Use collective.recipe.backup 1.6 and set enable_snapshotrestore = false on
  production. [maurits]

- Added zest_django_buildout template, based on fez.djangoskel. [vincent]


0.9 (2010-09-20)
----------------

- Add a new zest_buildout_plone4 template for, "well I never", a Plone 4
  buildout.
  [fred]

- add mr.developer example config [fred]

- Upgrade to varnish 2.1.3 and varnish install recipe 1.1 [fred]

- update TinyMCE, collective.recipe.backup
  [fred]

- add superlance plugin and memmon example (commented out).
  [fred]

- Use collective.recipe.supervisor 0.12 and use
  'buildout:directory/bin/zeoclient console' to start the instance.
  [maurits]

- Add awstats configuration template. [mark+fred]

- Add Nginx configuration template. [mark]

- In the plone buildout versions.cfg, pin mr.developer to 1.9 to avoid
  issues with older subversion clients (1.4); not used by default, but
  it is good to have this pin when we start using mr.developer in a
  project.
  [maurits]

- Add skeleton templates for a Django app. [mark]

- Use Plone 3.3.5. [mark]

- Use eggified Products.enablesettrace. [mark]

- Removed the default old-style products from the template. [mark]

- Changed default ports and added documentation how to handle multiple sites
  on one server. [jladage]

- Added conf part to both base.cfg and production.cfg to allow for easy
  configuration of ports, cache sizes, and cronjob timing. [jladage]

- Upgrade Products.TinyMCE to 1.1rc9 [fred]

- Add extra crontabs for backup and zeopack in production. Renamed crontab
  section to crontab-start. [fred]

- Add logrotation to the production buildout [jldaage]

- Update varnish recipe to not use a fixed url but let the recipe
  determine the download and use zc.recipe.cmmi for building the source.
  [fred]

- Use fresh list of debug tools, enabled by default or not, with
  versions pinned.  Note that some have been replaced by an egg
  instead of a subversion checkout.
  [maurits]

- Use customized version of enablesettrace (to also allow ipdb). [mark]

- Use Plone 3.3.4. [mark]

- New setup for the buildout configuration. [mark]

- Use zserver-threads = 2.  [maurits]

- Add TinyMCE as a default product since we use it all the time. [mark]

- Use newer version of bootstrap.py for the --distribute option and
  therefore also use zc.buildout 1.4.3. [mark]

- Change the debug tools [mark]:
  - Include and enable PrintingMailHost.
  - Disable clouseau and docfindertab.

- Use Plone 3.3.3. [mark]


0.8 (2009-08-04)
----------------

- Added z3c.recipe.usercrontab to production.cfg to start supervisord
  at reboot.  [maurits]


0.7 (2009-05-04)
----------------

- Used the buildout.dumppickedversions extension in the unstable
  buildout to list the unpinned products. [mark]

- Use Plone 3.2.2 in both stable and unstable. [mark]

- Added Products.Gloworm to the unstable eggs.  [maurits]

- Added bin/test (though without packages, so by default it runs all
  tests for all packages :-)) [maurits]

- Added proper omelette settings.  [maurits]

- Fixed typo in line to get the picked (not pinned) package versions.
  [maurits]

- Changed zest_buildout version.txt from '0.1 (svn/devel)' to '0.1
  dev' as that is the standard now.  [maurits]


0.6 (2008-12-05)
----------------

- Use plone.recipe.plone 3.1.7 in unstable and stable.  [maurits]

- Pin plone.recipe.varnish to 1.0rc4 as rc5 and rc6 give a config
  error and make varnish fail to start.  [maurits]

- Make the various config files more alike, copying some good ideas
  from one to another and removing unneeded stuff.  [maurits]

- Removed the already commented out CacheFu 1.1.1 from productdistros
  as 1.2 is already in the eggs section.  [maurits]

- stable: do not use the buildout products dir.  [maurits]


0.5 (2008-10-16)
----------------

- Removed iw.debug as this introduces lots of test failures.
  [maurits]

- Added commented PTS_LANGUAGES environment var hint [fred]

- Added more comments to the [version] sections of the (un)stable.cfg
  to match the layout suggested in
  http://vanrees.org/weblog/versions-buildout. [mark]

- Use plone 3.1.4 [mark]
- Use plone 3.1.5.1 [fred]

- Put omelette at the end of the sections so it picks up all products from
  previous sections [fred]

- Use Products.CacheSetup instead of the 1.1.1 tar.gz distribution [fred]

- Add plone.reload and iw.debug to the devel.cfg [fred]

- Add EXTERNALS.txt example in src [fred]

- Add backup, varnish and supervisor sections to production.cfg [fred, reinout]

- Make zeo-address in the instance section from production.cfg use the
  zeo-server section address [fred, reinout]

Version 0.3 - (16 May 2008)
---------------------------

- Use port 8080 in devel.cfg.  [maurits]

- Fixed bug with unknown pdbdebugmode location.  Added some more
  locations of debug tools to the instance.
