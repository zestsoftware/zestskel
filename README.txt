Introduction
============

ZestSkel provides a collection of skeletons for quickstarting Zope
and Plone projects.

All skeletons are available as PasteScript_ templates and can be used
via the ''paster'' commandline tool. For example to create a package
for a Plone 4 buildout you can do::

    paster create -t zest_buildout_plone4

this will ask a few questions such as desired package name and a description
and output a complete package skeleton that you can immediately start using.

Please contribute by submitting patches for what you consider 'best of
breed' file layouts for starting our projects.

.. _PasteScript: http://pythonpaste.org/script/
.. _Subversion repository: http://svn.zestsoftware.nl/svn/zest/internal/ZestSkel/


Available templates
===================

Deployment templates
--------------------

zest_buildout

zest_buildout_plone4

zest_django_app

zest_django_buildout
