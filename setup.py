from setuptools import setup, find_packages

version = '0.11.dev0'

setup(name='ZestSkel',
      version=version,
      description="A skeleton for quickstarting Zest projects.",
      long_description=open('README.txt').read() + "\n" +
                       open('HISTORY.txt').read(),
      classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Framework :: Zope2",
        "Framework :: Zope3",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
      keywords='web zope command-line skeleton project',
      author='Maurits van Rees',
      author_email='m.van.rees@zestsoftware.nl',
      url='http://zestsoftware.nl/',
      packages=find_packages(exclude=['ez_setup']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
        "PasteScript",
        "Cheetah",
      ],
      entry_points="""
      [paste.paster_create_template]
      zest_buildout = zestskel:ZestBuildout
      zest_buildout_plone4 = zestskel:ZestBuildoutPlone4
      zest_buildout_plone5 = zestskel:ZestBuildoutPlone5
      zest_buildout_plone51 = zestskel:ZestBuildoutPlone51
      zest_django_app = zestskel:ZestDjangoApp
      zest_django_buildout = zestskel:ZestDjangoBuildout
      """,
      )
