from paste.script.templates import Template, var
from random import choice

def make_secret():
    return ''.join([choice('abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)') for i in range(50)])

class ZestDjangoBuildout(Template):
    _template_dir = 'templates/zest_django_buildout'
    summary = "A Django buildout, Zest style (based on fez.djangoskel)"
    required_templates = []
    use_cheetah = True

    vars = [
        var('django_version',
            'Django version to fetch, the default is 1.4',
            default='1.4'),
        var('django_project_name',
            'Name of the main Django project folder',
            default='project'),
        var('secret',
            'Secret key used in settings',
            default=make_secret()),
        var('site_url',
            'URL of the website (no http or www)',
            default='example.com'),
    ]

    def post(self, command, output_dir, vars):
        print "-----------------------------------------------------------"
        print "Generation finished"
        print "-----------------------------------------------------------"
