import os

from zestskel.base import BaseTemplate
# from zestskel.base import var


class ZestBuildoutPlone5(BaseTemplate):
    category = "Plone Buildout"
    _template_dir = 'templates/zest_buildout_plone5'
    summary = "A buildout for Zest Plone 5 projects"
    required_templates = []
    use_cheetah = True
    use_local_commands = False  # or setup.cfg will be re created
    vars = [
        ]

    def post(self, command, output_dir, vars):
        # add gitignore
        base = os.path.join(output_dir)
        prj = vars['project']
        theme_path = os.path.join(base,'src', prj + '.theme', prj, 'theme', 'diazo_resources')
        hidden_files = [
            (base, 'gitignore'),
            (base, 'gitattributes'),
            (theme_path, 'jshintrc'),
        ]
        for path, name in hidden_files:
            try:
                os.rename(os.path.join(path, name),
                          os.path.join(path, '.{}'.format(name)))
            except OSError, e:
                msg = """WARNING: Could not create {} file: .{}""".format(name, name)
                self.post_run_msg = msg % str(e)
        # Make varnishd template executable.
        os.chmod(os.path.join(base, 'templates', 'varnishd.in'), 0744)

        print "-----------------------------------------------------------"
        print "Generation finished"
        print
        print "Please copy buildout.cfg.orig to buildout.cfg and edit it"
        print "to extend the correct buildout configuration"
        print "(devel.cfg or production.cfg)."
        print
        print "Then run python2.7 bootstrap.py and bin/buildout"
        print
        print "Or in case of version problems run:"
        print
        print "python2.7 bootstrap.py --buildout-version=2.2.1 --setuptools-version=1.4.2"
        print
        print "See README.txt for details"
        print
        print
        print "===================== IMPORTANT ==========================="
        print "===        Please read the above instructions.          ==="
        print "==========================================================="
