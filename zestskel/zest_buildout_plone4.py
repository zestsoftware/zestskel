import os

from zestskel.base import BaseTemplate
# from zestskel.base import var


class ZestBuildoutPlone4(BaseTemplate):
    category = "Plone Buildout"
    _template_dir = 'templates/zest_buildout_plone4'
    summary = "A buildout for Zest Plone 4 projects"
    required_templates = []
    use_cheetah = True
    use_local_commands = False  # or setup.cfg will be re created
    vars = [
        ]

    def post(self, command, output_dir, vars):
        # add gitignore
        path = os.path.join(output_dir)
        try:
            os.rename(os.path.join(path, 'gitignore'),
                      os.path.join(path, '.gitignore'))
        except OSError, e:
            msg = """WARNING: Could not create .gitignore file: %s"""
            self.post_run_msg = msg % str(e)
        # Make varnishd template executable.
        os.chmod(os.path.join(path, 'templates', 'varnishd.in'), 0744)

        print "-----------------------------------------------------------"
        print "Generation finished"
        print
        print "Please copy buildout.cfg.orig to buildout.cfg and edit it"
        print "to extend the correct buildout configuration"
        print "(devel.cfg or production.cfg)."
        print
        print "Then run python2.7 bootstrap.py and bin/buildout"
        print
        print "Or in case of version problems run:"
        print
        print "python2.7 bootstrap.py --buildout-version=2.2.1 --setuptools-version=1.4.2"
        print
        print "See README.txt for details"
        print
        print "Please note that after using this paster template the file "
        print "templates/varnishd.in should be executable, but might not be."
        print "Fix this with:"
        print "> chmod +x templates/varnishd.in"
        print
        print "===================== IMPORTANT ==========================="
        print "===        Please read the above instructions.          ==="
        print "==========================================================="
