from paste.script.templates import Template, var

class ZestDjangoApp(Template):
    _template_dir = 'templates/zest_django_app'
    summary = "A Django app, Zest style"
    required_templates = []
    use_cheetah = True

    vars = [
    ]

    def post(self, command, output_dir, vars):
        print "-----------------------------------------------------------"
        print "Generation finished"
        print "-----------------------------------------------------------"



