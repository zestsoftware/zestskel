from paste.script import templates
import copy

var = templates.var

LICENSE_CATEGORIES = {
    'GPL' : 'License :: OSI Approved :: GNU General Public License (GPL)',
    'LGPL' : 'License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)',
    'MIT' : 'License :: OSI Approved :: MIT License',
    'ZPL' : 'License :: OSI Approved :: Zope Public License',
    }

def get_var(vars, name):
    for var in vars:
        if var.name == name:
            return var
    else:
        raise ValueError("No such var: %r" % name)

class BaseTemplate(templates.Template):
    """Base template for all ZopeSkel templates"""
    _template_dir = 'templates/zest_buildout'
    summary = 'Create a Plone buildout Zest style'
    vars = copy.deepcopy(templates.Template.vars)


