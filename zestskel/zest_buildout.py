from zestskel.base import BaseTemplate
from zestskel.base import var

class ZestBuildout(BaseTemplate):
    _template_dir = 'templates/zest_buildout'
    summary = "A buildout for Zest Plone 3 projects"
    required_templates = []
    use_cheetah = True

    vars = [
        ]

    def post(self, command, output_dir, vars):
        print "-----------------------------------------------------------"
        print "Generation finished"
        print "You probably want to run python bootstrap.py and then edit"
        print "buildout.cfg before running bin/buildout -v"
        print
        print "See README.txt for details"
        print
        print "You may want to add these lines in svn propedit svn:ignore ."
        print ".mr.developer.cfg"
        print "eggs"
        print "fake-eggs"
        print "downloads"
        print "develop-eggs"
        print "buildout.cfg"
        print "parts"
        print "bin"
        print "var"
        print ".installed.cfg"
        print "src"
        print "-----------------------------------------------------------"



