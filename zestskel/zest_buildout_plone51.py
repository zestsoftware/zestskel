import os

from zestskel.base import BaseTemplate
# from zestskel.base import var


class ZestBuildoutPlone51(BaseTemplate):
    category = "Plone Buildout"
    _template_dir = 'templates/zest_buildout_plone51'
    summary = "A buildout for Zest Plone 5.1 projects"
    required_templates = []
    use_cheetah = True
    use_local_commands = False  # or setup.cfg will be re created
    vars = [
        ]

    def post(self, command, output_dir, vars):
        # add gitignore
        base = os.path.join(output_dir)
        prj = vars['project']
        hidden_files = [
            (base, 'gitignore'),
            # (base, 'gitattributes'),
        ]
        for path, name in hidden_files:
            try:
                os.rename(os.path.join(path, name),
                          os.path.join(path, '.{}'.format(name)))
            except OSError, e:
                msg = """WARNING: Could not create {} file: .{}""".format(name, name)
                self.post_run_msg = msg % str(e)
        # Make varnishd template executable.
        os.chmod(os.path.join(base, 'templates', 'varnishd.in'), 0744)

        print "-----------------------------------------------------------"
        print "Generation finished"
        print
        print "Please copy buildout.cfg.orig to buildout.cfg and edit it"
        print "to extend the correct buildout configuration"
        print "(devel.cfg or production.cfg)."
        print
        print "Then look at bootstrap_.me.sh and see if this installs your"
        print "virtualenv for python2.7 correctly"
        print
        print
        print "===================== IMPORTANT ==========================="
        print "===        Please read the above instructions.          ==="
        print "==========================================================="
