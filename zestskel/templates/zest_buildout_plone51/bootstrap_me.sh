#!/bin/bash
# Create a virtualenv in the buildout directory
# possible xtra options: --never-download (behind firewall/dmz)
virtualenv --python=python2.7 .
# install zc.buildout and latest compatible setuptools for buildout
bin/pip install --upgrade -r requirements.txt
