# Run this with:
# bin/instance run scripts/disable_cache.py 
#
# Add --dry-run to change nothing and only get a report.

import sys
import transaction
from plone import api
from zope.component.hooks import setSite

if '--dry-run' in sys.argv:
    dry_run = True
    print('Dry run selected, will not commit changes.')
else:
    dry_run = False

# Get all Plone Sites.  'app' is the Zope root.
plones = [obj for obj in app.objectValues()
          if getattr(obj, 'portal_type', '') == 'Plone Site']


def commit(note):
    print(note)
    if dry_run:
        print('Dry run selected, not committing.')
        return
    # Commit transaction and add note.
    tr = transaction.get()
    tr.note(note)
    transaction.commit()


for site in plones:
    print('')
    print('Handling Plone Site %s.' % site.id)
    setSite(site)
    # Disable caching on localhost
    api.portal.set_registry_record(
        'plone.caching.interfaces.ICacheSettings.enabled',
        False)
    note = ('Finished disabling cache for %s' % site.id)
    commit(note)
    print('Done.')

