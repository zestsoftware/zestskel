from plone.dexterity.content import Item
from zope.interface import Interface, implements


class IYourContentType(Interface):
    pass


class YourContentType(Item):
    implements(IYourContentType)
