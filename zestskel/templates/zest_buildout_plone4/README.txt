===================
Using this buildout
===================

For general comments about using buildout see these resources:

http://www.buildout.org/

http://developer.plone.org/reference_manuals/active/deployment/index.html

http://developer.plone.org/reference_manuals/old/buildout/index.html


Getting started
---------------

The first thing you need to do is decide if you are going to use this
buildout for development, preview or production.  You need to copy
buildout.cfg.orig to buildout.cfg and edit that to extend devel.cfg,
preview.cfg or production.cfg.

 $ cp buildout.cfg.orig buildout.cfg

Any other .cfg files you see are used by devel/preview/production.cfg
and should not be used directly.

For backgrounds, see this weblog entry and the other documentation it
points to:

http://maurits.vanrees.org/weblog/archive/2008/01/easily-creating-repeatable-buildouts


The preferred way of running local python applications is by using a virtualenv. 
When installing the virtualenv make sure you install a Python 2.7 version. 

If you know your virtualenv willd default to a Python 2.7:

# virtualenv .

If you're unsure: specify the path to a python2.7 executable:

$ virtualenv -p /usr/local/bin/python2.7 .

Now you can use pip to install zc.buildout & setuptools:

$ bin/pip install -r requirements.txt

To create an instance you now run:

 $ bin/buildout

This will download Plone's eggs for you, as well as other
dependencies, create a new Zope instance configured with these
packages, and possibly create some config files, a backup script,
cronjobs, etcetera.

You can start your Zope instance by running:

 $ bin/instance start

or, to run in foreground mode for developing:

 $ bin/instance fg


Upgrading a preview/production buildout
---------------------------------------

Stop all services:

 $ bin/supervisorctl shutdown

Make a backup of the Data.fs (database) and blobstorage:

 $ bin/backup

Now switch the buildout to the tag given by Zest, e.g.:

 $ git fetch
 $ git checkout 0.1

Run bin/buildout (this will get any new packages and does other needed
changes):

 $ bin/buildout

Watch for any errors.

Start the services:

 $ bin/supervisord

Watch for any errors in the instance log file:

 $ tail -f var/log/zeoclient.log

Now you may need to (re)install some products in the Plone Site
control panel.  If unsure, ask Zest Software.


For general comments about using buildout see the top of this file.
