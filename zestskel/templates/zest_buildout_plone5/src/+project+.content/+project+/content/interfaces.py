from zope.interface import Interface


class IDefaultContent(Interface):
    """Marker interface to mark default content.

    We apply this to our generated content: the main folders and their
    default pages.  We use it to hide the display dropdown menu, so no
    one can erroneously change the display, as they cannot switch it
    back unless they go to the ZMI.
    """
