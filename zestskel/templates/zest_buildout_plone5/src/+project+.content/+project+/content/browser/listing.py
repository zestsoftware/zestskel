from plone import api
from Products.Five import BrowserView


class YourContentTypeListing(BrowserView):

    def yourcontenttypes(self):
        catalog = api.portal.get_tool(name='portal_catalog')
        yourcontenttypes = []
        for brain in catalog(portal_type='yourcontenttype'):
            yourcontenttypes.append(brain.getObject())
        return yourcontenttypes
